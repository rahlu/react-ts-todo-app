import { Reducer } from "redux";
import { Action } from "../models";

// Casting assert to const variable -> see typescript-3-4 docs

// This pattern helps to reduce boilerplate so that switch cases are not required and you can just perform a lookup of all the action types
const createReducer = <S>(initialState: S, handlers: any): Reducer<S> => {
  console.log(handlers);
  const reducer = (state: S = initialState, action: Action): S =>
    handlers.hasOwnProperty(action.type)
      ? handlers[action.type](state, action)
      : state;
  return reducer as Reducer<S>;
};

export default createReducer;
