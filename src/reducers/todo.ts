import { TodoActionType, TodoAction, TodoActions, Todo } from "../models";
import createReducer from "./createReducer";

type TodoState = Todo[];

const initialState = [
  { id: 1, text: "Clean the dishes", complete: false },
  { id: 2, text: "Walk the dog", complete: true },
  { id: 3, text: "Code an app", complete: false },
  { id: 4, text: "Complete redux integration", complete: true },
];

interface TodoActionUpdate {
  payload: {
    id: number;
    text: string;
    complete: boolean;
  };
}

export const todos = createReducer<TodoState>(initialState, {
  [TodoActions.ADD_TODO](state: TodoState, action: TodoAction) {
    return [...state, action.payload];
  },
  [TodoActions.UPDATE_TODO](state: TodoState, action: TodoAction) {
    console.log(action.payload);
    return state.map((todo) =>
      todo.id === action.payload.id
        ? { ...todo, text: action.payload.text }
        : todo
    );
  },
  [TodoActions.TOGGLE_COMPLETE](state: TodoState, action: TodoAction) {
    return state.map((todo) =>
      todo.id === action.payload ? { ...todo, complete: !todo.complete } : todo
    );
  },
  [TodoActions.REMOVE_TODO](state: TodoState, action: TodoAction) {
    return state.filter((todo) => todo.id !== action.payload);
  },
});
