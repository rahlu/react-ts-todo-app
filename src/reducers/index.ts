import { combineReducers } from "redux";
import { todos } from "./todo";

const rootReducer = combineReducers({ todos });

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
