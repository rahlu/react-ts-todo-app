import React from "react";
import { connect } from "react-redux";
import Todos from "../components/Todos";
import { AppState } from "../reducers";
import { Todo } from "../models/todo";

interface HomeProps {
  todos: Todo[];
}

const Home = ({ todos }: HomeProps) => {
  return (
    <div>
      <h4>All items</h4>
      <Todos todos={todos} />
    </div>
  );
};

const mapStateToProps = (state: AppState) => ({
  todos: state.todos,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
