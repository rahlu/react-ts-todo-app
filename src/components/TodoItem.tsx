import React from "react";
import ListGroup from "react-bootstrap/ListGroup";
import { Todo } from "../models/todo";

type TodoItemProps = {
  todo: Todo;
};

const TodoItem: React.FC<TodoItemProps> = ({ todo }) => {
  const { text, complete } = todo;

  return <ListGroup.Item>{text}</ListGroup.Item>;
};

export default TodoItem;
