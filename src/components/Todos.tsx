import React from "react";
import TodoItem from "./TodoItem";
import ListGroup from "react-bootstrap/ListGroup";
import { Todo } from "../models/todo";

type TodoProps = {
  todos: Todo[];
  complete?: boolean;
};

const getTodos = (todos: Array<Todo>, complete: boolean | undefined) => {
  if (typeof complete !== "undefined") {
    todos = todos.filter((todo) => todo.complete === complete);
  }
  return todos.map((todo) => <TodoItem key={todo.id} todo={todo} />);
};

const Todos: React.FC<TodoProps> = ({ todos, complete }: TodoProps) => {
  const todosToRender = getTodos(todos, complete);
  return <ListGroup>{todosToRender}</ListGroup>;
};

export default Todos;
