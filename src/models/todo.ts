export interface Todo {
  id: number;
  text: string;
  complete: boolean;
}

export enum TodoActions {
  ADD_TODO = "ADD_TODO",
  UPDATE_TODO = "UPDATE_TODO",
  TOGGLE_COMPLETE = "TOGGLE_COMPLETE",
  REMOVE_TODO = "REMOVE_TODO",
}

export interface TodoActionType<T, P> {
  type: T;
  payload: P;
}

export type TodoAction =
  | TodoActionType<typeof TodoActions.UPDATE_TODO, Todo>
  | TodoActionType<typeof TodoActions.ADD_TODO, Todo>
  // | TodoActionType<typeof TodoActions.TOGGLE_COMPLETE, number>
  // | TodoActionType<typeof TodoActions.REMOVE_TODO, number>;

  // Need to figure out this section ^