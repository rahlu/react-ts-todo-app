import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes";
import NavBar from "./components/NavBar";

type AppProps = {};

type AppState = {};

class App extends React.Component<AppProps, AppState> {
  render() {
    return (
      <div>
        <NavBar />
        <Router>
          <Routes />
        </Router>
      </div>
    );
  }
}

export default App;
